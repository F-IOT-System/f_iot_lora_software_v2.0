/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    subghz_phy_app.c
  * @author  MCD Application Team
  * @brief   Application of the SubGHz_Phy Middleware
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "platform.h"
#include "sys_app.h"
#include "subghz_phy_app.h"
#include "radio.h"

/* USER CODE BEGIN Includes */
#include "stm32wlxx_hal.h"
#include "stm32_timer.h"
#include "stm32_seq.h"
#include "utilities_def.h"
#include "stdint.h"
#include "stdbool.h"
#include "string.h"

//#define GATEWAY		//Uncommand when set lora is Gateway mode
#define CLIENT   //Uncommand when set lora is Client mode

//#define TESTER	//Uncommand when tester

#define Lora_ID 2

#define MAX_APP_BUFFER_SIZE           255

#ifdef GATEWAY
#define RX_TIMEOUT_VALUE              50
#endif

#ifdef CLIENT
#define SEND_TIME_DELAY 			  2
#define RX_TIMEOUT_VALUE              2000
#endif

#define TX_TIMEOUT_VALUE              2000

#define UARTRX_BUFF_SIZE 50

#define HandShake_Code 0xF1
#define Sync_Ok_Code 0xA0
#define Sync_Error_Code 0xE0

#define LOST_CHECK 10

#define MAX_SIZE_UARTSEND 5
#define MAX_SIZE_UARTRECV 5
#define MAX_SIZE_STORE 50
/* USER CODE END Includes */

/* External variables ---------------------------------------------------------*/
/* USER CODE BEGIN EV */
static uint8_t uartSend_Store[MAX_SIZE_UARTSEND][50];
static uint8_t uartSend_Store_Count[MAX_SIZE_UARTSEND];
static uint8_t uartSend_Store_Size = 0;

static uint8_t uartReciver_buffer[UARTRX_BUFF_SIZE];
static uint8_t uartReciver_Store[MAX_SIZE_UARTRECV][50];
static uint8_t uartRecive_Store_Count[MAX_SIZE_UARTRECV];
static uint8_t uartReciver_Store_Size = 0;


static uint8_t radio_reciver_Package[50];
static uint8_t radio_reciver_Package_Size;
static int8_t radio_reciver_SNR;

static	uint8_t Lora_Now_ID = Lora_ID;


static	uint8_t Lora_send_Package[50];
static	uint8_t Lora_send_Package_Size;

static uint8_t uart_payload[50];

#ifdef GATEWAY
static	uint8_t Connected[3] = {0,0,0};

static	uint8_t Count_Data_Store[3][MAX_SIZE_STORE];
static	uint8_t Size_Store[3] = {0,0,0};
static	uint8_t DATA_Store[3][MAX_SIZE_STORE][50];

static 	uint8_t sync_code[2] = {0xA0,0xA0};

static uint8_t Radio_Lost_Cout[3];
#endif

#ifdef CLIENT

static bool Connected = false;

static	uint8_t Count_Data_Store[MAX_SIZE_STORE];
static	uint8_t Size_Store = 0;
static	uint8_t DATA_Store[MAX_SIZE_STORE][50];
static 	uint8_t check_handshake_count;
static  uint8_t sync_code ;

#endif

static bool packet_send;

#ifdef TESTER
//#error //set value in debug mode and command this line
static uint64_t Radio_Package_Send;
static uint64_t Radio_Package_Send_Error;

static uint64_t Radio_Package_Reciver;
static uint64_t Radio_Package_Reciver_Error;

static uint64_t Uart_Package_Send;
static uint64_t Uart_Package_Send_Error;

static uint64_t Uart_Package_Reciver;
static uint64_t Uart_Package_Reciver_Error;

static uint64_t Radio_HandShake_Count[3];
#endif
/* USER CODE END EV */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */


extern IWDG_HandleTypeDef hiwdg;
extern CRC_HandleTypeDef hcrc;
extern UART_HandleTypeDef huart1;
extern DMA_HandleTypeDef hdma_usart1_tx;
extern DMA_HandleTypeDef hdma_usart1_rx;
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* Radio events function pointer */
static RadioEvents_t RadioEvents;

/* USER CODE BEGIN PV */

static UTIL_TIMER_Object_t Eventtimeout_Radio;
static UTIL_TIMER_Object_t Eventtimeout_LED_TX;
static UTIL_TIMER_Object_t Eventtimeout_LED_RX;
static UTIL_TIMER_Object_t Eventtimeout_Uart_Error;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/*!
 * @brief Function to be executed on Radio Tx Done event
 */
static void OnTxDone(void);

/**
  * @brief Function to be executed on Radio Rx Done event
  * @param  payload ptr of buffer received
  * @param  size buffer size
  * @param  rssi
  * @param  LoraSnr_FskCfo
  */
static void OnRxDone(uint8_t *payload, uint16_t size, int16_t rssi, int8_t LoraSnr_FskCfo);

/**
  * @brief Function executed on Radio Tx Timeout event
  */
static void OnTxTimeout(void);

/**
  * @brief Function executed on Radio Rx Timeout event
  */
static void OnRxTimeout(void);

/**
  * @brief Function executed on Radio Rx Error event
  */
static void OnRxError(void);

/* USER CODE BEGIN PFP */

static void Timeout_Radio(void *context);

static void Timeout_LED_TX(void *context);
static void Timeout_LED_RX(void *context);

static void Timeout_Uart_Error(void *context);

static void uartSend_DMA(uint8_t* Pdata, uint8_t size);
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart);

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size);
static void Serial_Process(void);
static void Uart_Package_Process(uint8_t* uartPackage , uint8_t uartPackageCout);
//static void Serial_ISR(uint8_t *PData, uint16_t Size, uint8_t Error);

static void Radio_Reciver_Process(void);

static void Radio_Send_Data(void);
static void Clear_Data_Store(void);
static void Have_Reciver_Data(uint8_t *payload, uint16_t size, int8_t LoraSnr_FskCfo);
static void MX_IWDG_Init(void);
/* USER CODE END PFP */

/* Exported functions ---------------------------------------------------------*/
void SubghzApp_Init(void)
{
  /* USER CODE BEGIN SubghzApp_Init_1 */
#ifdef GATEWAY
  	for(int i=0;i<5;i++)
  	{
  		HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_9);
  		HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_10);
  		HAL_Delay(500);
  	}

#endif
	UTIL_TIMER_Init();

	UTIL_TIMER_Create(&Eventtimeout_Radio,RX_TIMEOUT_VALUE, UTIL_TIMER_ONESHOT, Timeout_Radio, NULL);
	UTIL_TIMER_Create(&Eventtimeout_LED_TX,200, UTIL_TIMER_ONESHOT, Timeout_LED_TX, NULL);
	UTIL_TIMER_Create(&Eventtimeout_LED_RX,200, UTIL_TIMER_ONESHOT, Timeout_LED_RX, NULL);
	UTIL_TIMER_Create(&Eventtimeout_Uart_Error,10, UTIL_TIMER_ONESHOT, Timeout_Uart_Error, NULL);


  /* USER CODE END SubghzApp_Init_1 */

  /* Radio initialization */
  RadioEvents.TxDone = OnTxDone;
  RadioEvents.RxDone = OnRxDone;
  RadioEvents.TxTimeout = OnTxTimeout;
  RadioEvents.RxTimeout = OnRxTimeout;
  RadioEvents.RxError = OnRxError;

  Radio.Init(&RadioEvents);

  /* USER CODE BEGIN SubghzApp_Init_2 */
  Radio.SetChannel(RF_FREQUENCY);

  	Radio.SetTxConfig(MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
  			  LORA_SPREADING_FACTOR, LORA_CODINGRATE,
  			  LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
  			  true, 0, 0, LORA_IQ_INVERSION_ON, TX_TIMEOUT_VALUE);
  	Radio.SetRxConfig(MODEM_LORA, LORA_BANDWIDTH, LORA_SPREADING_FACTOR,
  			  LORA_CODINGRATE, 0, LORA_PREAMBLE_LENGTH,
  			  LORA_SYMBOL_TIMEOUT, LORA_FIX_LENGTH_PAYLOAD_ON,
  			  0, true, 0, 0, LORA_IQ_INVERSION_ON, true);
  	Radio.SetMaxPayloadLength(MODEM_LORA, MAX_APP_BUFFER_SIZE);

  	UTIL_SEQ_RegTask((1<<CFG_SEQ_Task_Vcom), UTIL_SEQ_RFU, Serial_Process);
  	UTIL_SEQ_RegTask((1<<CFG_SEQ_Task_CheckConnect), UTIL_SEQ_RFU, Radio_Send_Data);
  	UTIL_SEQ_RegTask((1<<CFG_SEQ_Task_SubGHz_Phy_App_Process), UTIL_SEQ_RFU, Radio_Reciver_Process);


  	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_SET);
  	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);

  	HAL_UARTEx_ReceiveToIdle_DMA(&huart1, uartReciver_buffer, UARTRX_BUFF_SIZE);
  	 __HAL_DMA_DISABLE_IT(&hdma_usart1_rx,DMA_IT_HT);

#ifdef GATEWAY
  	MX_IWDG_Init();
  	UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_CheckConnect), CFG_SEQ_Prio_1);
#endif

#ifdef CLIENT
  	Radio.RxBoosted(0);
#endif
  /* USER CODE END SubghzApp_Init_2 */
}

/* USER CODE BEGIN EF */

/* USER CODE END EF */

/* Private functions ---------------------------------------------------------*/
static void OnTxDone(void)
{
  /* USER CODE BEGIN OnTxDone */
#ifdef TESTER
	if(packet_send){
		Radio_Package_Send++;
		Radio_Package_Send_Error++;
	}
#endif

	memset(Lora_send_Package,0,Lora_send_Package_Size);
	Lora_send_Package_Size = 0;


	HAL_IWDG_Refresh(&hiwdg);

	UTIL_TIMER_Start(&Eventtimeout_Radio);
	Radio.RxBoosted(0);


  /* USER CODE END OnTxDone */
}

static void OnRxDone(uint8_t *payload, uint16_t size, int16_t rssi, int8_t LoraSnr_FskCfo)
{
  /* USER CODE BEGIN OnRxDone */

	Radio.Standby();
	if(size >= 50){
		size = 50;
	}
	memcpy(radio_reciver_Package,payload,size);
	radio_reciver_Package_Size = size;
	radio_reciver_SNR = LoraSnr_FskCfo;
	UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_SubGHz_Phy_App_Process), CFG_SEQ_Prio_1);

  /* USER CODE END OnRxDone */
}

static void OnTxTimeout(void)
{
  /* USER CODE BEGIN OnTxTimeout */
#ifdef CLIENT
	Radio.RxBoosted(0);
#endif
  /* USER CODE END OnTxTimeout */
}

static void OnRxTimeout(void)
{
  /* USER CODE BEGIN OnRxTimeout */
#ifdef CLIENT
	Radio.RxBoosted(0);
#endif
  /* USER CODE END OnRxTimeout */
}

static void OnRxError(void)
{
  /* USER CODE BEGIN OnRxError */
#ifdef TESTER
	Radio_Package_Reciver_Error++;
#endif
#ifdef GATEWAY
	sync_code[Lora_Now_ID-1] = Sync_Error_Code;
	UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_CheckConnect), CFG_SEQ_Prio_1);
#endif

#ifdef CLIENT
	Lora_send_Package[0] = Sync_Error_Code;
	Lora_send_Package[1] = Lora_Now_ID;
	Lora_send_Package_Size = 2;
	Radio.Send(Lora_send_Package,Lora_send_Package_Size);
#endif
  /* USER CODE END OnRxError */
}

/* USER CODE BEGIN PrFD */
#ifdef GATEWAY
static void Timeout_Radio(void *context)
{
	packet_send = false;
	if(Connected[Lora_Now_ID-1] == Lora_Now_ID){

		Connected[Lora_Now_ID-1] = 0;


		uart_payload[0] = Lora_Now_ID;
		uart_payload[1] = 7;//cmd
		uart_payload[2] = 0;
		uart_payload[3] = 1;//length
		uart_payload[4] = 0;//status
		uint16_t crc_caculator = (~HAL_CRC_Calculate(&hcrc, (uint32_t*)uart_payload,5))&0xffff;

		uart_payload[5] = crc_caculator  & 0xff;
		uart_payload[6] = (crc_caculator >> 8) & 0xff;
		uartSend_DMA(uart_payload,7);

	}


	UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_CheckConnect), CFG_SEQ_Prio_1);
}
#endif

#ifdef CLIENT
void Timeout_Radio(void *context)
{

	Connected = false;
	packet_send = false;
	uart_payload[0] = Lora_Now_ID;
	uart_payload[1] = 7;//cmd
	uart_payload[2] = 1;//length
	uart_payload[3] = 0;//data

	uint16_t crc_caculator = (~HAL_CRC_Calculate(&hcrc, (uint32_t*)uart_payload,4))&0xffff;

	uart_payload[4] = crc_caculator & 0xff;
	uart_payload[5] = (crc_caculator >> 8) & 0xff;

	uartSend_DMA(uart_payload,6);

	Radio.RxBoosted(0);
}
#endif
static void Timeout_LED_TX(void *context)
{
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_SET);
}

static void Timeout_LED_RX(void *context)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);
}

static void Timeout_Uart_Error(void *context){
	uint8_t uart_error;
	uart_error = HAL_UART_Transmit_DMA(&huart1, &uartSend_Store[0][0], uartSend_Store_Count[0]);
	if((uart_error == HAL_BUSY) || (uart_error == HAL_ERROR)){
		UTIL_TIMER_Start(&Eventtimeout_Uart_Error);
	}
#ifdef TESTER
		Uart_Package_Send_Error++;
#endif

}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){

	if(huart->Instance == USART1){


		memset(&uartSend_Store[0][0],0,uartSend_Store_Count[0]);
		uartSend_Store_Count[0] = 0;

		if(uartSend_Store_Size > 0){
			uartSend_Store_Size--;
		}



		if(uartSend_Store_Size > 0){

			for(int i = 0 ; i < uartSend_Store_Size ; i++){
				memcpy(&uartSend_Store[i][0],&uartSend_Store[i+1][0],uartSend_Store_Count[i+1]);
				uartSend_Store_Count[i] = uartSend_Store_Count[i+1];
			}
			uint8_t uart_error;
			uart_error = HAL_UART_Transmit_DMA(&huart1, &uartSend_Store[0][0], uartSend_Store_Count[0]);
			if((uart_error == HAL_BUSY) || (uart_error == HAL_ERROR)){
				UTIL_TIMER_Start(&Eventtimeout_Uart_Error);
			}

		}
#ifdef TESTER
		Uart_Package_Send++;
#endif
	}
}

static void uartSend_DMA(uint8_t* Pdata, uint8_t size){

	uint8_t uart_error;

	uartSend_Store_Count[uartSend_Store_Size] = size;
	memcpy(&uartSend_Store[uartSend_Store_Size][0],Pdata,size);
	uartSend_Store_Size++;
	if(uartSend_Store_Size >= MAX_SIZE_UARTSEND){
		uartSend_Store_Size = MAX_SIZE_UARTSEND - 1;
	}

	if(uartSend_Store_Size <= 1){

		uart_error = HAL_UART_Transmit_DMA(&huart1, &uartSend_Store[0][0], uartSend_Store_Count[0]);

		if((uart_error == HAL_BUSY) || (uart_error == HAL_ERROR)){
			uartSend_Store_Count[0] = size;
			memcpy(&uartSend_Store[0][0],Pdata,size);
			uartSend_Store_Size++;
			if(uartSend_Store_Size >= MAX_SIZE_UARTSEND){
				uartSend_Store_Size = MAX_SIZE_UARTSEND-1;
			}
			UTIL_TIMER_Start(&Eventtimeout_Uart_Error);
		}
	}
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t Size){
	if(huart->Instance == USART1){
		if(Size >= 50){
			Size = 50;
		}
		memcpy(&uartReciver_Store[uartReciver_Store_Size][0],uartReciver_buffer,Size);
		uartRecive_Store_Count[uartReciver_Store_Size] = Size;

		uartReciver_Store_Size++;
		if(uartReciver_Store_Size >= MAX_SIZE_UARTRECV){
			uartReciver_Store_Size = MAX_SIZE_UARTRECV - 1;
		}

		UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_Vcom), CFG_SEQ_Prio_0);

		HAL_UARTEx_ReceiveToIdle_DMA(&huart1, uartReciver_buffer, UARTRX_BUFF_SIZE);
		__HAL_DMA_DISABLE_IT(&hdma_usart1_rx,DMA_IT_HT);

	}
}

static void Serial_Process(void){

	for(int i = 0 ; i < uartReciver_Store_Size ; i++){
		Uart_Package_Process(&uartReciver_Store[i][0],uartRecive_Store_Count[i]);
#ifdef TESTER
		Uart_Package_Reciver++;
#endif
	}
	memset(uartRecive_Store_Count,0,uartReciver_Store_Size);
	uartReciver_Store_Size = 0;


}


#ifdef GATEWAY
static void Uart_Package_Process(uint8_t* uartPackage , uint8_t uartPackageCout)
{

		uint16_t crc_payload = uartPackage[uartPackageCout-1];
		crc_payload <<= 8;
		crc_payload |= uartPackage[uartPackageCout-2];

		uint16_t crc_check = (~HAL_CRC_Calculate(&hcrc, (uint32_t*)uartPackage, uartPackageCout-2))&0xffff;

		if((crc_payload == crc_check) && (uartPackageCout > 2)){

			if(uartPackage[0] == Connected[uartPackage[0]-1]){

				memcpy(&DATA_Store[uartPackage[0]-1][Size_Store[uartPackage[0]-1]][0],uartPackage,uartPackageCout);

				Count_Data_Store[uartPackage[0]-1][Size_Store[uartPackage[0]-1]] = uartPackageCout; //byte Sync

				Size_Store[uartPackage[0]-1]++;
				if(Size_Store[uartPackage[0]-1] >= MAX_SIZE_STORE){
					Size_Store[uartPackage[0]-1] = MAX_SIZE_STORE-1;
				}

			}else{

				uart_payload[0] = uartPackage[0];
				uart_payload[1] = 7;//cmd
				uart_payload[2] = 0;
				uart_payload[3] = 1;//length
				uart_payload[4] = 0;//status

				uint16_t crc_caculator = (~HAL_CRC_Calculate(&hcrc, (uint32_t*)uart_payload,5))&0xffff;

				uart_payload[5] = crc_caculator  & 0xff;
				uart_payload[6] = (crc_caculator >> 8) & 0xff;

				uartSend_DMA(uart_payload,7);

			}
		}
#ifdef TESTER
		else{
			Uart_Package_Reciver_Error++;
		}
#endif

}
#endif


#ifdef CLIENT
static void Uart_Package_Process(uint8_t* uartPackage , uint8_t uartPackageCout)
{
		uint16_t crc_payload = uartPackage[uartPackageCout - 1];
		crc_payload <<= 8 ;
		crc_payload |= uartPackage[uartPackageCout - 2];

		uint16_t crc_check = (~HAL_CRC_Calculate(&hcrc,(uint32_t*)uartPackage, uartPackageCout-2))&0xffff;

		if((crc_check == crc_payload) && (uartPackageCout > 2) && Connected)
		{

			uartPackage[0] = Lora_Now_ID;
			uint16_t crc_caculator = (~HAL_CRC_Calculate(&hcrc,(uint32_t*)uartPackage, uartPackageCout-2))&0xffff;
			uartPackage[uartPackageCout-2] = crc_caculator & 0xff;
			uartPackage[uartPackageCout-1] = (crc_caculator>> 8) & 0xff;

			Count_Data_Store[Size_Store] = uartPackageCout;
			memcpy(&DATA_Store[Size_Store][0],uartPackage ,uartPackageCout);
			Size_Store++;
			if(Size_Store >= MAX_SIZE_STORE){
				Size_Store = MAX_SIZE_STORE - 1;
			}
		}
#ifdef TESTER
		else if((crc_check != crc_payload) && (uartPackageCout < 2)){
			Uart_Package_Reciver_Error++;
		}
#endif
}
#endif


static void Radio_Reciver_Process(void){

	if(radio_reciver_Package[1] == Lora_Now_ID){

		UTIL_TIMER_Stop(&Eventtimeout_Radio);


			switch(radio_reciver_Package[0]){

				case HandShake_Code:{
#ifdef TESTER
					Radio_HandShake_Count[Lora_Now_ID-1]++;
#endif
#ifdef GATEWAY

					Connected[Lora_Now_ID-1] = Lora_Now_ID;

					Size_Store[Lora_Now_ID-1] = 0;
					uart_payload[0] = Lora_Now_ID;
					uart_payload[1] = 7;//cmd
					uart_payload[2] = radio_reciver_SNR + 50;
					uart_payload[3] = 1;//length
					uart_payload[4] = 1;//status

					uint16_t crc_caculator = (~HAL_CRC_Calculate(&hcrc,(uint32_t*)uart_payload,5)) & 0xffff;

					uart_payload[5] = crc_caculator  & 0xff;
					uart_payload[6] = (crc_caculator >> 8) & 0xff;

					uartSend_DMA(uart_payload,7);
#endif

//#ifdef CLIENT
//
//					Size_Store = 0;
//					uart_payload[0] = Lora_Now_ID;
//					uart_payload[1] = 7;//cmd
//					uart_payload[2] = 1;//length
//					uart_payload[3] = 1;//data
//
//					uint16_t crc_caculator = (~HAL_CRC_Calculate(&hcrc, (uint32_t*)uart_payload,4))&0xffff;
//					uart_payload[4] = crc_caculator & 0xff;
//					uart_payload[5] = (crc_caculator>> 8) & 0xff;
//
//					uartSend_DMA(uart_payload,6);
//
//#endif

				}
				break;

				case Sync_Ok_Code:{ // byte Sync ok
					//if recevie packet from STM

					Have_Reciver_Data(radio_reciver_Package,radio_reciver_Package_Size,radio_reciver_SNR);

					Clear_Data_Store();


				}
				break;
				case Sync_Error_Code:{// byte Sync error

					Have_Reciver_Data(radio_reciver_Package,radio_reciver_Package_Size,radio_reciver_SNR);

				}
				break;

				default :
				break;

			}

			UTIL_SEQ_SetTask((1 << CFG_SEQ_Task_CheckConnect), CFG_SEQ_Prio_1);
		}
#ifdef CLIENT
		else{
			Radio.RxBoosted(0);
		}
#endif
//		memset(radio_reciver_Package,0,radio_reciver_Package_Size);
//		radio_reciver_Package_Size = 0;
//		radio_reciver_SNR = 0;

}


#ifdef GATEWAY
static void Radio_Send_Data(void)
{


	Lora_Now_ID++;

	if(Lora_Now_ID > 2){
		Lora_Now_ID = 1;
	}



	if(Connected[Lora_Now_ID-1] != Lora_Now_ID){
		Radio_Lost_Cout[Lora_Now_ID]++;
		if(Radio_Lost_Cout[Lora_Now_ID] < LOST_CHECK){
			Lora_Now_ID++;
			if(Lora_Now_ID > 2){
				Lora_Now_ID = 1;
			}
		}
		else{

			Radio_Lost_Cout[Lora_Now_ID] = 0;
		}
	}


	if(Lora_Now_ID == Connected[Lora_Now_ID-1]){

		if(Size_Store[Lora_Now_ID-1] > 0){

			Lora_send_Package[0] = sync_code[Lora_Now_ID-1];

			memcpy(&Lora_send_Package[1],&DATA_Store[Lora_Now_ID-1][0][0],Count_Data_Store[Lora_Now_ID-1][0]);
			Lora_send_Package_Size = Count_Data_Store[Lora_Now_ID-1][0] + 1;
			Radio.Send(Lora_send_Package,Lora_send_Package_Size);

			packet_send = true;

			UTIL_TIMER_Start(&Eventtimeout_LED_TX);
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_RESET);
		}
		else{

			Lora_send_Package[0] = sync_code[Lora_Now_ID-1];
			Lora_send_Package[1] = Lora_Now_ID;
			Lora_send_Package_Size = 2;
			Radio.Send(Lora_send_Package,Lora_send_Package_Size);
		}
	}

	else{
		Lora_send_Package[0] = HandShake_Code;
		Lora_send_Package[1] = Lora_Now_ID;
		Lora_send_Package_Size = 2;
		Radio.Send(Lora_send_Package,Lora_send_Package_Size);;
	}
}
#endif


#ifdef CLIENT
static void Radio_Send_Data(void){
//	HAL_Delay(SEND_TIME_DELAY);
	if(!Connected){
		MX_IWDG_Init();
		check_handshake_count++;
		Connected = true;
		Lora_send_Package[0] = HandShake_Code;
		Lora_send_Package[1] = Lora_Now_ID;
		Lora_send_Package_Size = 2;
		Radio.Send(Lora_send_Package,Lora_send_Package_Size);
	}
	else if(Size_Store > 0){

		memcpy(&Lora_send_Package[1],&DATA_Store[0][0],Count_Data_Store[0]);

		Lora_send_Package[0] = sync_code;

		Lora_send_Package_Size = Count_Data_Store[0] + 1;
		packet_send = true;

		UTIL_TIMER_Start(&Eventtimeout_LED_TX);
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_RESET);

		Radio.Send(Lora_send_Package,Lora_send_Package_Size);
	}
	else{

		Lora_send_Package[0] = sync_code;
		Lora_send_Package[1] = Lora_Now_ID;
		Lora_send_Package_Size = 2;
		Radio.Send(Lora_send_Package,Lora_send_Package_Size);
	}
}
#endif



#ifdef GATEWAY
static void Clear_Data_Store(void){

	if((Size_Store[Lora_Now_ID-1] > 0) && packet_send){

		memset(&DATA_Store[Lora_Now_ID-1][0][0],0,Count_Data_Store[Lora_Now_ID-1][0]);

		for(int i= 0; i < Size_Store[Lora_Now_ID-1] ; i++)
		{
			memcpy(&DATA_Store[Lora_Now_ID-1][i][0],&DATA_Store[Lora_Now_ID-1][i+1][0],Count_Data_Store[Lora_Now_ID-1][i+1]);
		}
		//decrease size and delete count

		memcpy(&Count_Data_Store[Lora_Now_ID-1][0],&Count_Data_Store[Lora_Now_ID-1][1],Size_Store[Lora_Now_ID-1]);

		memset(&DATA_Store[Lora_Now_ID-1][Size_Store[Lora_Now_ID-1]][0],0,Count_Data_Store[Lora_Now_ID-1][Size_Store[Lora_Now_ID-1]]);
		Count_Data_Store[Lora_Now_ID-1][Size_Store[Lora_Now_ID-1]] = 0;

		if(Size_Store[Lora_Now_ID-1] > 0){
			Size_Store[Lora_Now_ID-1]--;
		}


		packet_send = false;

#ifdef TESTER
		if(Radio_Package_Send_Error > 0){
			Radio_Package_Send_Error--;
		}
#endif
	}

}
#endif


#ifdef CLIENT
static void Clear_Data_Store(void){

	if((Size_Store > 0) && packet_send){
		memset(&DATA_Store[0][0],0,Count_Data_Store[0]);
		for(int i = 0 ; i < Size_Store ; i++)
		{
			memcpy(&DATA_Store[i][0],&DATA_Store[i+1][0],Count_Data_Store[i+1]);
		}
		memcpy(&Count_Data_Store[0],&Count_Data_Store[1],Size_Store);

		memset(&DATA_Store[Size_Store][0],0,Count_Data_Store[Size_Store]);
		Count_Data_Store[Size_Store] = 0 ;

		if(Size_Store > 0){
			Size_Store--;
		}


		packet_send = false;

#ifdef TESTER
		if(Radio_Package_Send_Error > 0){
			Radio_Package_Send_Error--;
		}
#endif

	}

}
#endif


#ifdef GATEWAY
static void Have_Reciver_Data(uint8_t *payload, uint16_t size, int8_t LoraSnr_FskCfo){



	if(size > 2){

		UTIL_TIMER_Start(&Eventtimeout_LED_RX);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);

		memcpy(uart_payload,&payload[1],2);

		uart_payload[2] = LoraSnr_FskCfo + 50;

		memcpy(&uart_payload[3],&payload[3],payload[3]+1);

		uint16_t crc_caculator = (~HAL_CRC_Calculate(&hcrc, (uint32_t*)uart_payload,uart_payload[3] + 4))&0xffff;

		uart_payload[uart_payload[3] + 4] = crc_caculator & 0xff;
		uart_payload[uart_payload[3] + 5] = (crc_caculator >> 8) & 0xff;


		uartSend_DMA(uart_payload,uart_payload[3] + 6);

#ifdef TESTER
		Radio_Package_Reciver++;
#endif

	}

	sync_code[Lora_Now_ID-1] = Sync_Ok_Code;
}
#endif


#ifdef CLIENT
static void Have_Reciver_Data(uint8_t *payload, uint16_t size, int8_t LoraSnr_FskCfo){

	if(size > 2){

		UTIL_TIMER_Start(&Eventtimeout_LED_RX);
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);

		uartSend_DMA(&payload[1], size - 1);
#ifdef TESTER
		Radio_Package_Reciver++;
#endif
	}
	sync_code = Sync_Ok_Code;
}

#endif

static void MX_IWDG_Init(void)
{

  /* USER CODE BEGIN IWDG_Init 0 */

  /* USER CODE END IWDG_Init 0 */

  /* USER CODE BEGIN IWDG_Init 1 */

  /* USER CODE END IWDG_Init 1 */
  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_32;
  hiwdg.Init.Window = 2999;
  hiwdg.Init.Reload = 2999;
  if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
  	{
  	Error_Handler();
  	}
  /* USER CODE BEGIN IWDG_Init 2 */

  /* USER CODE END IWDG_Init 2 */

}

/* USER CODE END PrFD */
